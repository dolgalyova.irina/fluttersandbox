import 'package:flutter/material.dart';

ThemeData createTheme() => ThemeData(primarySwatch: Colors.blueGrey);

const double padding = 16.0;