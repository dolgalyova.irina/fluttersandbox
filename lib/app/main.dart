import 'package:flutter/material.dart';
import 'package:flutter_sandbox/app/App.dart';

void main() => runApp(MaterialApp(
  home: new App(),
));
