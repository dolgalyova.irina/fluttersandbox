import 'package:flutter/material.dart';
import 'package:flutter_sandbox/app/AppTheme.dart';
import 'package:flutter_sandbox/screen/followers/FollowersScreen.dart';


class App extends StatelessWidget {
  static const String AppName = "Followers";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: AppName,
      theme: createTheme(),
      home: FollowersScreen.create(),
    );
  }
}