
import 'package:flutter_sandbox/screen/followers/FollowersView.dart';
import 'package:flutter_sandbox/screen/followers/domain/FollowersInteractor.dart';
import 'package:flutter_sandbox/screen/followers/rest/model/User.dart';
import 'package:flutter_sandbox/screen/followers/router/FollowersRouter.dart';

class FollowersPresenter {
  final FollowersInteractor _interactor;
  final FollowersRouter _router;
  FollowersView _view;

  FollowersPresenter(this._interactor, this._router);

  set view(FollowersView value) {
    this._view = value;
    _loadData();
  }

  void onItemClick(User user) {
    _router.openUserDetails(user.id.toString());
  }

  void _loadData() {
    _interactor.getFollowers((List<User> result) {
      _view.setData(result);
    });
  }
}