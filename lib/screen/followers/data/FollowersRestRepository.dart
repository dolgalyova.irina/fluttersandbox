import 'package:flutter_sandbox/screen/followers/data/FollowersRepository.dart';
import 'package:flutter_sandbox/screen/followers/rest/FollowersApi.dart';
import 'package:flutter_sandbox/screen/followers/rest/model/User.dart';

class FollowersRestRepository implements FollowersRepository {
  final FollowersApi _api;

  FollowersRestRepository(this._api);

  @override
  Future<List<User>> getUsers(int page) => _api.getUser(page);
}
