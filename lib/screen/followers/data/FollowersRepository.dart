
import 'package:flutter_sandbox/screen/followers/rest/model/User.dart';

abstract class FollowersRepository {
  Future<List<User>> getUsers(int page);
}
