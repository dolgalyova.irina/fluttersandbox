import 'package:flutter/cupertino.dart';
import 'package:flutter_sandbox/screen/followers/FollowersPresenter.dart';
import 'package:flutter_sandbox/screen/followers/data/FollowersRestRepository.dart';
import 'package:flutter_sandbox/screen/followers/domain/FollowersInteractor.dart';
import 'package:flutter_sandbox/screen/followers/rest/FollowersRestApi.dart';
import 'package:flutter_sandbox/screen/followers/router/FollowersMaterialRouter.dart';

class FollowersInjector {
  FollowersPresenter buildPresenter(BuildContext context) {
    var api = FollowersRestApi();
    var repository = FollowersRestRepository(api);
    var interactor = FollowersInteractor(repository);
    var router = FollowersMaterialRouter(context);
    return FollowersPresenter(interactor, router);
  }
}
