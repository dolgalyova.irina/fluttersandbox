import 'package:flutter_sandbox/screen/followers/data/FollowersRepository.dart';
import 'package:flutter_sandbox/screen/followers/rest/model/User.dart';

class FollowersInteractor {
  final FollowersRepository _repository;

  FollowersInteractor(this._repository);

  void getFollowers(onSuccess(List<User> result), {onError(Exception error)}) {
    _repository.getUsers(0).then(onSuccess).catchError(onError);
  }
}
