import 'package:flutter/material.dart';
import 'package:flutter_sandbox/screen/details/FollowerDetailsScreen.dart';
import 'package:flutter_sandbox/screen/followers/router/FollowersRouter.dart';

class FollowersMaterialRouter implements FollowersRouter {
  final BuildContext _context;

  FollowersMaterialRouter(this._context);

  @override
  void openUserDetails(String id) {
    Navigator.push(_context,
        MaterialPageRoute(builder: (_) => FollowerDetailsScreen.create(id)));
  }
}