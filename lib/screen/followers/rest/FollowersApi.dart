import 'package:flutter_sandbox/screen/followers/rest/model/User.dart';

abstract class FollowersApi {

  Future<List<User>> getUser(int page);
}