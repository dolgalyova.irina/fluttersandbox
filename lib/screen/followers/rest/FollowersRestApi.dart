import 'package:flutter_sandbox/screen/followers/rest/FollowersApi.dart';
import 'package:flutter_sandbox/screen/followers/rest/model/User.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'dart:io';

class FollowersRestApi implements FollowersApi {
  final String BASE_URL = "https://api.github.com/users/JakeWharton/followers";

  @override
  Future<List<User>> getUser(int page) async {

    final response = await http.get(BASE_URL);

    if (response.statusCode == 200) {
      Iterable parsed = jsonDecode(response.body);

      List<User> results = parsed.map((model) => User.fromJson(model)).toList();

      return results;
    } else {
      throw Exception('Failed to load post');
    }
  }
}
