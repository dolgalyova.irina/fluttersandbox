class User{
  final int id;
  final String name;
  final String photo;

  User({this.id, this.name, this.photo});


  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      name: json['login'],
      photo: json['avatar_url'],
    );
  }
}