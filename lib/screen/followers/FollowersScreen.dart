import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sandbox/screen/followers/FollowersPresenter.dart';
import 'package:flutter_sandbox/screen/followers/di/FollowersInjector.dart';
import 'package:flutter_sandbox/screen/followers/view/FollowersListView.dart';

class FollowersScreen extends StatelessWidget {
  static FollowersScreen create() => FollowersScreen();

  FollowersPresenter _presenter;

  Widget build(BuildContext context) {
    _presenter = FollowersInjector().buildPresenter(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Followers"),
      ),
      body: FollowersListView(
        presenter: _presenter,
      ),
    );
  }
}
