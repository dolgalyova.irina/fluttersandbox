import 'package:flutter/material.dart';
import 'package:flutter_sandbox/screen/followers/FollowersPresenter.dart';
import 'package:flutter_sandbox/screen/followers/FollowersView.dart';
import 'package:flutter_sandbox/screen/followers/rest/model/User.dart';
import 'package:flutter_sandbox/screen/followers/view/FollowerItemView.dart';

class FollowersListView extends StatefulWidget {
  final FollowersPresenter presenter;

  const FollowersListView({Key key, this.presenter}) : super(key: key);

  @override
  State<StatefulWidget> createState() => FollowersListState(presenter);
}

class FollowersListState extends State<FollowersListView>
    implements FollowersView {
  final FollowersPresenter presenter;
  List<User> followers = new List();

  FollowersListState(this.presenter) {
    presenter.view = this;
  }

  @override
  void setData(List<User> data) {
    setState(() {
      this.followers = data;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget widget;
    if (followers.isNotEmpty) {
      widget = ListView(
        padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
        children: buildItems(),
      );
    } else {
      widget = Center(
        child: CircularProgressIndicator(),
      );
    }
    return widget;
  }

  List<FollowerItemView> buildItems() => followers.map((User item) {
    return FollowerItemView(item, presenter.onItemClick);
  }).toList();
}
