import 'package:flutter/material.dart';
import 'package:flutter_sandbox/app/AppTheme.dart';
import 'package:flutter_sandbox/screen/followers/rest/model/User.dart';

class FollowerItemView extends StatelessWidget {
  final Function(User) _onClick;
  final User user;

  const FollowerItemView(this.user, this._onClick, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextTheme textTheme = Theme.of(context).textTheme;
    return GestureDetector(
        onTap: () => _onClick(user),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image(image: NetworkImage(user.photo), width: 96, height: 64),
            ),
            Padding(
              padding: const EdgeInsets.only(left: padding, right: padding),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    user.name,
                    textAlign: TextAlign.start,
                    style: textTheme.subhead,
                  ),
                  Text(
                    user.id.toString(),
                    maxLines: 1,
                    textAlign: TextAlign.start,
                    style: textTheme.body2,
                  )
                ],
              ),
            )
          ],
        ));
  }
}
