import 'package:flutter_sandbox/screen/followers/rest/model/User.dart';

abstract class FollowersView {

  void setData(List<User> data);
}