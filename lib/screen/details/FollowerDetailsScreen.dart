import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FollowerDetailsScreen extends StatelessWidget {
 String id;

  FollowerDetailsScreen(String id){
    this.id = id;
  }

  static FollowerDetailsScreen create(String id) => FollowerDetailsScreen(id);


  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Follower details " + id.toString()),
      )
    );
  }
}
